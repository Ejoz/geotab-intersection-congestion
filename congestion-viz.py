import streamlit as st
import pandas as pd
import numpy

st.title('Geotab Intersection Congestion')


# @st.cache(ignore_hash=True)
def download_data(csv_file):
    data = pd.read_csv(csv_file, sep=',')
    return data


df = download_data('data/train.csv')
st.write(df.head(20))

cities = df['City'].unique()
st.write('How many cities are in the dataset ?', cities)


def display_map(data, city):
    city_data = pd.DataFrame(
        {'lat': data['Latitude'][data['City'] == city], 'lon': data['Longitude'][data['City'] == city]})

    return st.map(city_data)


for city in cities:
    st.subheader(city)
    display_map(df, city)
